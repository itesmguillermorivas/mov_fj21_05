package com.example.fj_2021_5;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private static final int PERMISSION_LOCATION = 0;

    // fused location api
    private static final long UPDATE = 10 * 1000;
    private static final long FASTEST = 2 * 1000;
    private Location lastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // fused location
        LocationRequest request = new LocationRequest();

        // SET PRECISION?
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        request.setInterval(UPDATE);
        request.setFastestInterval(FASTEST);

        // check permissions to see if you can actually do this
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED){

            LocationServices.getFusedLocationProviderClient(this).requestLocationUpdates(
                    request,
                    new LocationCallback(){

                        public void onLocationResult(LocationResult locationResult){

                            lastLocation = locationResult.getLastLocation();
                            Log.wtf("LOCATION", lastLocation.toString());
                        }
                    },
                    Looper.myLooper()
            );


        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng campus = new LatLng(20.736994, -103.454209);
        mMap.addMarker(new MarkerOptions().position(campus).title("EIAD Guadalajara"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(campus, 18));
        mMap.setOnMapClickListener(this);

        // intention: enable the location layer for the user
        // mMap.setMyLocationEnabled(true);
        enableMyLocation();
    }

    @Override
    public void onMapClick(LatLng latLng) {

        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("dynamic pin!")
                .alpha(0.5f)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
        );
    }

    private void enableMyLocation() {

        // before enabling my location check for permissions!
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED){

            // request permission
            String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
            requestPermissions(permissions, PERMISSION_LOCATION);

        } else {

            // permission was already granted! :D
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == PERMISSION_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            Toast.makeText(this, "PERMISSION GRANTED! :D", Toast.LENGTH_SHORT).show();

            if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED)
            mMap.setMyLocationEnabled(true);

        } else {

            Toast.makeText(this, "PERMISSION DENIED! :(", Toast.LENGTH_SHORT).show();
        }
    }
}